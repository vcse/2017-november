<!DOCTYPE html>
<html>
<head>
	<title>Ruander- PHP haladó tanfolyam</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Ruander- PHP haladó tanfolyam - OOP</h1>
		<div class="row">
			<div class="col-xs-12">
				<?php// include 'test.php'; ?>
				<?php include "form.php" ; ?>
			</div>
		</div>
		
		<footer class="row">
			<div class="col-xs-12">
				Ruander &copy; <?php echo date('Y-m-d'); ?>
			</div>
		</footer>
	</body>	
	</html>	