<?php
//osztály betöltése
use Address\AddressResidence as AddressResidence;
use Address\AddressBusiness as AddressBusiness;
use Address\AddressTemporary as AddressTemporary;
use Address\Address as Address;
use Address\ExceptionAddress as ExceptionAddress;



//osztály betöltése
function __autoload($name){

	$loadName=str_replace('\\',DIRECTORY_SEPARATOR,$name);
	echo $name.'->'.$loadName;
	
	//$class_name="class.$name.inc";
	$class_name="$loadName.inc";
	include $class_name;

}


echo '<h2>Objektum készítése asszociatív tömbből a konstruktor használatával</h2>';

$data=[
	'address_line_1'=>'teszt utca2.',
	'city_name' =>'Szentendre',
	//'country_name'=>'Magyarország',
	//'address_type_id'=>1
	//'postal_code'=>5555
];



//$address_2=new Address($data);
//echo '<pre>'.var_export($address_2,true).'</pre>';


//echo '<hr>';
//echo $address_2;


echo '<h2>osztály bővítések</h2>';
$address_residence=new AddressResidence($data);
echo '<pre>'.var_export($address_residence,true).'</pre>';
echo $address_residence;

$address_business=new AddressBusiness($data);
echo '<pre>'.var_export($address_business,true).'</pre>';
echo $address_business;
$address_business->save();

$address_temporary=new AddressTemporary($data);
echo '<pre>'.var_export($address_temporary,true).'</pre>';
echo $address_temporary;
$address_temporary->save();
echo '<pre>'.var_export($address_temporary,true).'</pre>';

try{
$address_db=Address::load(5);
echo '<pre>'.var_export($address_db,true).'</pre>';
$address_db->delete();
}catch (Exception $e){
	//echo $e->getMessage();
	echo $e;
}
?>
