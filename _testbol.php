<?php
//osztály betöltése
include "class.Address.inc";
include "class.Database.inc";

echo '<h2>Üres objektum létrehozása</h2>';
$address=new Address;

echo '<pre>'.var_export($address,true).'</pre>';

echo '<h2>Objektum tulajdonságok feltöltése adatokkal</h2>';
$address->address_line_1='Teszt utca 1.';
$address->city_name='Szentendre';
$address->country_name='Magyarország';
$address->address_type_id='5';
//$address->valami='hacked';
$address->postal_code=2000;
echo '<pre>'.var_export($address,true).'</pre>';

echo '<h2>Objektum eljárás hívása a cím kiírására</h2>';
echo $address->display();
echo '<h2>Objektum tulajdonság kiírása</h2>';
$address->country_name;

//echo '<h2>Objektum védett tulajdonság tesztelése-(protected)</h2>';
//$address->_postal_code;

echo '<h2>Objektum készítése asszociatív tömbből a konstruktor használatával</h2>';

$data=[
	'address_line_1'=>'teszt utca2.',
	'city_name' =>'Szentendre',
	'country_name'=>'Magyarország',
	'address_type_id'=>1
	//'postal_code'=>5555
	];
$address_2=new Address($data);
echo '<pre>'.var_export($address_2,true).'</pre>';


echo '<hr>';
echo $address_2;
echo '<hr>';
echo '<pre>'.var_export($address_2,true).'</pre>';

//statikus oszt tulajdonság teszt
echo '<h2>statikus osztály tulajdonság teszt</h2>';

foreach(Address::$valid_address_types as $type){
	echo $type.',';
}

echo '<h2>konstans teszt</h2>';
echo 'residence azonosító:'.Address::ADDRESS_TYPE_RESIDENCE;

echo '<h2>típuskényszerítés teszt-typecasting</h2>';
$test='Hello World';
echo '<pre>'.var_export($test,true).'</pre>';
echo '<pre>'.var_export((int)$test,true).'</pre>';
echo '<pre>'.var_export((object)$test,true).'</pre>';
$data=[
	0=>'test',
	'helo'=>pi(),
	'nested'=>[
		'valami','más valami'],
	100=>1000
	];
$object=(object)$data;
echo '<pre>'.var_export($object,true).'</pre>';
//elemek elérése(asszociatív kulcs tulajdonságként elérhető)
echo $object->helo;

echo '<h2>statikus eljárás tesztelése</h2>';
for($i=0;$i<=5;$i++){
	echo '<br>'.$i.' -> '.(Address::isValidAddressTypeId($i) ? '':'nem').' érvényes';
}
?>